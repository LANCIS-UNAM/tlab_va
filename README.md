# tlab_va

Laboratorio de Transformación Socio-Ecológica (T-Lab) hacia la construcción del
Segundo Plan de Acción Local de Gobierno Abierto del estado de Veracruz, 
Capítulo Temático "Agroecología"
[T-Lab Veracruz Agroecológico]

Este proyecto adapta elementos del enfoque T-Lab (Pathways Network 2018) 
para abordar el caso de la agroecología en el estado de Veracruz, México.

En este repositorio se encuentran las bases de datos de resultados de los 
Laboratorios de Transformación (carpetas T-Lab1 y T-Lab2). 